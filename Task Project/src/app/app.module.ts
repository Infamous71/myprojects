import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {DepartmentService} from "./core/department.service";
import {LocalStorageService} from "ngx-store";
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from "@angular/router";
import {appRoutes} from "./app.routes";
import {DepartmentEmployeesComponent} from "./department-employees/deparment-employees.component";
import {EmployeeService} from "./core/employee.service";
import {NoSelectedDepartmentComponent} from "./no-selected-department/no-selected-department.component";
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    AppComponent,
    DepartmentEmployeesComponent,
    NoSelectedDepartmentComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [DepartmentService, EmployeeService, LocalStorageService],
  bootstrap: [AppComponent]
})
export class AppModule {
}

