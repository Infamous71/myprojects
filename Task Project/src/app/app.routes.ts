import {DepartmentEmployeesComponent} from "./department-employees/deparment-employees.component";
import {NoSelectedDepartmentComponent} from "./no-selected-department/no-selected-department.component";


export const appRoutes = [
  {
    path: '',
    component: NoSelectedDepartmentComponent

  },
  {
    path: 'department/:id',
    component: DepartmentEmployeesComponent
  }
]
