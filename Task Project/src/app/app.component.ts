import {Component, OnInit} from '@angular/core';
import {DepartmentService} from "./core/department.service";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  departmentForm: FormGroup;

  id: number;
  name: string;
  description: string;

  showMenu: boolean = false;

  departments: any[];

  selectedDepartment: number = null;

  constructor(private departmentService: DepartmentService,
              private formBuilder: FormBuilder,
              private router: Router) {

  }

  ngOnInit() {
    this.loadDepartments();
    this.buildForm();
  }

  buildForm() {
    this.departmentForm = this.formBuilder.group(
      {
        id: [this.id],
        name: [this.name],
        description: [this.description]
      }
    )
  }

  prepareData() {
    this.id = this.departmentForm.get("id").value;
    this.name = this.departmentForm.get("name").value;
    this.description = this.departmentForm.get("description").value;
  }

  showAddMenu() {
    this.showMenu = !this.showMenu;
  }

  loadDepartments() {
    this.departmentService.getDepartments()
      .subscribe(resp => {
        console.log("Resp:", resp);
        this.departments = resp.body;
      })
  }

  loadEmployeesInDepartment(depId: number) {
    this.selectedDepartment = depId;
    this.router.navigate(["/department", depId])
  }

  addDepartment() {
    this.prepareData();
    this.departmentService.addDepartment({
      "id": this.id,
      "name": this.name,
      "description": this.description
    }).subscribe(resp => this.loadDepartments())

    console.log(this.id, this.name, this.description)

  }

  removeDepartment(depId: number) {
    this.departmentService.removeDepartment(depId)
      .subscribe(resp => this.loadDepartments())
  }

  loadStartPage() {
    this.router.navigate([""])
  }
}
