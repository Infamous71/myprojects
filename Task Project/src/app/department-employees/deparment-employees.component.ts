import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {EmployeeService} from "../core/employee.service";

@Component({
  selector: 'department-employees',
  templateUrl: './department-employees.component.html',
  styleUrls: ['./department-employees.component.css']
})


export class DepartmentEmployeesComponent {
  employees: any[];
  departmentId: number;

  constructor(private route: ActivatedRoute, private employeeService: EmployeeService) {
    this.employees = [];
    this.route.params.subscribe(params => {
      this.departmentId = params['id'];
      this.loadEmployees();
    })

  }

  loadEmployees() {
    this.employeeService.getEmployees().subscribe(
      resp => {
        this.getEmployeesForDepartment(resp.body);
      }
    )
  }

  getEmployeesForDepartment(emp: any[]) {
    this.employees = [];
    for (let k of emp) {
      if (k.departmentId == this.departmentId) {
        this.employees.push(k);
      }
    }
  }

}
