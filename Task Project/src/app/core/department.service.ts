import {DEPARTMENTS_URL} from "./urls";
import {LocalStorageService} from "ngx-store";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class DepartmentService {
  constructor(private localStorageService: LocalStorageService,
              private  httpClient: HttpClient) {
  }

  getDepartments(): Observable<any> {
    return this.httpClient.get<any>(DEPARTMENTS_URL, {observe: "response"});
  }

  addDepartment(credentials: any): Observable<any> {
    return this.httpClient.post(DEPARTMENTS_URL, credentials, {observe: "response"})
  }

  removeDepartment(id: number): Observable<any> {
    return this.httpClient.delete(DEPARTMENTS_URL + id, {observe: "response"})
  }
}
