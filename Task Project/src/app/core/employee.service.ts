import {EMPLOYEE_URL} from "./urls";
import {LocalStorageService} from "ngx-store";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class EmployeeService {

  constructor(private localStorageService: LocalStorageService,
              private httpClient: HttpClient) {

  }

  getEmployees(): Observable<any> {
    return this.httpClient.get<any>(
      EMPLOYEE_URL, {observe: 'response'}
    );
  }
}
